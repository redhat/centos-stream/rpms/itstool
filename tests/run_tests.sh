#!/bin/bash -e

# Debugging: Ensure the file exists
echo "Checking for /usr/share/doc/itstool/examples/test.xml..."
ls -l /usr/share/doc/itstool/examples/test.xml || exit 1

# Proceed with the test
cp /usr/share/doc/itstool/examples/test.xml /tmp/itstool_test/
itstool /tmp/itstool_test/test.xml -o /tmp/itstool_test/test.generated.po
diff /tmp/itstool_test/test.expected.po /tmp/itstool_test/test.generated.po

# Define directories for output
OUTPUT_DIR="/tmp/itstool_test"
mkdir -p "$OUTPUT_DIR"

# Test data
TEST_XML="/usr/share/doc/itstool/examples/test.xml"
EXPECTED_PO="$OUTPUT_DIR/test.expected.po"
GENERATED_PO="$OUTPUT_DIR/test.generated.po"
NORMALIZED_PO="$OUTPUT_DIR/test.normalized.po"

# Create expected output
cat > "$EXPECTED_PO" <<EOF
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\\n"
"POT-Creation-Date: DATE_PLACEHOLDER"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\\n"
"Language-Team: LANGUAGE <LL@li.org>\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"

#. (itstool) path: root/greeting
#: test.xml:2
msgid "Hello, World!"
msgstr ""

EOF

# Copy test XML file
cp "$TEST_XML" "$OUTPUT_DIR/test.xml"

# Run itstool to generate PO file
itstool -o "$GENERATED_PO" "$OUTPUT_DIR/test.xml"

# Normalize the generated PO file
sed -e "s|POT-Creation-Date: .*|POT-Creation-Date: DATE_PLACEHOLDER\"|" \
    -e "s|#: .*test.xml|#: test.xml|g" \
    "$GENERATED_PO" > "$NORMALIZED_PO"

# Compare normalized PO file with expected output
if diff -u "$EXPECTED_PO" "$NORMALIZED_PO"; then
    echo "PASS: itstool successfully generated the expected PO file."
else
    echo "FAIL: The generated PO file does not match the expected output."
    echo "Expected: $EXPECTED_PO"
    echo "Generated: $NORMALIZED_PO"
    exit 1
fi

